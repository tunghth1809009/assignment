#include <stdio.h>
#include <stdbool.h>
#include <mem.h>
#include <stdlib.h>



typedef struct student{
    char Masinhvien[10];
    char Tensinhvien[50];
    char Sodienthoai[15];
};

long numberStudent = 1;
struct student sinhvien[1];

void themmoisinhvien() {
    printf("Vui long nhap ma sinh vien: \n");
    fgetc(stdin);
    fgets(sinhvien[0].Masinhvien, 5 + 1 , stdin);

    if (!strchr(sinhvien[0].Masinhvien, '\n')) {
        while (fgetc(stdin) != '\n');
    }
    int countInput = strlen(sinhvien[0].Masinhvien);
    printf("%d",countInput);

    while (countInput != 5) {
        printf("Nhap ma sinh vien (nhap du 5 ki tu).\n");
        getchar();
        fgets(sinhvien[0].Masinhvien, 5, stdin);
        if (!strchr(sinhvien[0].Masinhvien, '\n')) {
            while (fgetc(stdin) != '\n');
        }
    }

    printf("Vui long nhap ten sinh vien: \n");
    fgetc(stdin);
    fgets(sinhvien[0].Tensinhvien, 50, stdin);
    if (!strchr(sinhvien[0].Tensinhvien, '\n')) {
        while (fgetc(stdin) != '\n');
    }

    printf("Vui long nhap so dien thoai: \n");
    fgetc(stdin);
    fgets(sinhvien[0].Sodienthoai, 15, stdin);
    if (!strchr(sinhvien[0].Sodienthoai, '\n')) {
        while (fgetc(stdin) != '\n');
    }

}

void hienthi(){
    printf("%-10s%-20s%-10s%-20s%-10s%-20s", "STT", "|" "Ma sinh vien", "|", "Ten sinh vien", "|", "So dien thoai" "\n");
    for (int i = 0; i < 10; ++i) {
        printf("\n");
        printf("%-10d%-20s%-10s%-20s%-10s%-20s", i + 1, sinhvien->Masinhvien, "|", sinhvien->Tensinhvien, "|", sinhvien->Sodienthoai);
    }
}
void boxuongdong(char *mang) {
    mang[strlen(mang) - 1] = ' ';
}

void docfile() {
    FILE *fp;
    printf("_______Read file data______\n");
    fp = fopen("danhsachsinhvien.txt", "r");
    int bufferSize = 10;
    char buffer[bufferSize];
    while (fgets(buffer, bufferSize, fp) != NULL) {
        printf("%s", buffer);
    }
}


void luufile() {
    FILE *fp = fopen("danhsachsinhvien.txt", "w+");
    fprintf(fp, "-%-20s|%-20s|%s", "Ma sinh vien ", "Ten sinh vien", "So dien thoai");
    while (true) {
        fgets(sinhvien, 10, stdin);
        if (strcmp(sinhvien, "exit\n") == 0) {
            break;
        }
        fprintf(fp, sinhvien);
    }
    fclose(fp);
}

void thoatfile() {
    int choice;
    printf("Ban muon nhap tiep khong (y/n)\n");
    scanf("%d", &choice);
    if (choice == 'y' || choice == 'Y') {
        exit(1);
    }
}

void menu(){
    int choice;
    while (true) {
        printf("------MENU------\n");
        printf("1.Them moi sinh vien.\n");
        printf("2.Hien thi danh sach sinh vien.\n");
        printf("3.Luu danh sach sinh vien ra file.\n");
        printf("4.Doc danh sach sinh vien ra file.\n");
        printf("5.Thoat chuong trinh\n");
        printf("Vui long nhap lua chon cua ban:\n");
        scanf("%d", &choice);
        switch (choice){
            case 1:
                themmoisinhvien();
                menu();
                break;
            case 2:
                hienthi();
                menu();
                break;
            case 3:
                docfile();
                menu();
                break;
            case 4:
                luufile();
                menu();
                break;
            case 5:
                thoatfile();
                break;
            default:
                printf("Ban da nhap sai vui long nhap lai.\n");
        }
        break;
    }
}


int main() {
    printf("Vui long nhap so luong sinh vien: \n");
    scanf("%d", &numberStudent);
    if(numberStudent < 1 || numberStudent > 10) {
        printf("Vui long nhap lai: \n");
    }

    menu();
    themmoisinhvien();
    hienthi();
    boxuongdong();
    docfile();
    luufile();
    return 0;
}